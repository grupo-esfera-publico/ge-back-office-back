# Docker
## Comando para construir
`docker build . -t grupo-esfera/back-office-be`

## Comando para correr
`docker run --name ge-be -p 3000:3000 grupo-esfera/back-office-be`

## Pasos para subir la imagen al Registry de gitlab (se pueden copiar de https://gitlab.com/grupo-esfera/productos/ge-back-office-be/container_registry)
`docker login registry.gitlab.com (con el username y password propio de gitlab)`
`docker build -t registry.gitlab.com/grupo-esfera/productos/ge-back-office-be .`
`docker push registry.gitlab.com/grupo-esfera/productos/ge-back-office-be`

## Comandos para subir y correr en un server de Esfera
`ssh forky@192.168.88.103 (-> con clave de forky)`

`su -l (-> con clave de root)`

`docker login registry.gitlab.com (Estando logueado en el gitlab)`

`docker run --name app-reservas-oficina-be -d -p 3000:3000 registry.gitlab.com/grupo-esfera/productos/ge-back-office-be:latest`

## Comando para actualizar la imagen a la última versión subida a la registry en un server de Esfera
`docker pull registry.gitlab.com/grupo-esfera/productos/ge-back-office-be:latest`

## Cuenta de Sendgrid
`notificaciones-covid@grupoesfera.com.ar`
`esfera2022Notificaciones`