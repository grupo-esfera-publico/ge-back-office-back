FROM node:16-alpine
WORKDIR /usr/src/app
COPY package*.json ./

RUN yarn install
COPY ./src ./src
COPY tsconfig.json ./
COPY .env ./
EXPOSE 3000
RUN yarn build
CMD [ "yarn", "start" ]