import { FirebaseBooking } from '../config/types';
import { formatterBookings } from './formatter';

describe('Formatter Tests', () => {
   describe('Formatter Bookings Tests', () => {
      it('With dates and bookings should return people per date collection', () => {
         const dates = [
            {
               weekDay: 'Lunes',
               day: 15,
               month: 0,
               year: 2022,
            },
         ];

         const secondsOfDate =
            new Date('2022-01-15T15:35:58.000Z').getTime() / 1000;

         const bookings = [
            new FirebaseBooking(
               '353535rwrw',
               { seconds: secondsOfDate, nanoseconds: 0 },
               'Guillermo Sfoggia',
               1,
               'Siempre temprano',
               'guillote.sfoggia@grupoesfera.com.ar',
            ),
         ];

         const bookingsPerDate = formatterBookings(dates, bookings);

         expect(bookingsPerDate[0].weekDay).toEqual('Lunes');
         expect(bookingsPerDate[0].dateObject).toEqual({
            day: 15,
            month: 0,
            year: 2022,
         });
         expect(bookingsPerDate[0].date).toEqual('15/1/2022');
         expect(bookingsPerDate[0].assistants).toEqual([
            {
               icon: '🍖',
               userDisplayName: 'Guillermo Sfoggia',
               id: '353535rwrw',
               keyArrivalTime: 'Siempre temprano',
               userEmail: 'guillote.sfoggia@grupoesfera.com.ar',
            },
         ]);
      });
   });
});
