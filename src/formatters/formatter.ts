import { FirebaseBooking, BookingsPerDate } from '../config/types';
import { FOOD_OPTIONS } from '../config/constants';

export const formatterBookings = (
   dates: any[],
   bookings: FirebaseBooking[],
): BookingsPerDate[] => {
   const peoplePerDate: BookingsPerDate[] = dates.map((ubd) => {
      return {
         weekDay: ubd.weekDay,
         date: ubd.day + '/' + (ubd.month + 1) + '/' + ubd.year,
         dateObject: { day: ubd.day, month: ubd.month, year: ubd.year },
         assistants: bookings
            .filter((i) => {
               const date = new Date(i.date.seconds * 1000);
               return (
                  date.getFullYear() === ubd.year &&
                  date.getMonth() === ubd.month &&
                  date.getDate() === ubd.day
               );
            })
            .map((j) => {
               const userFood = FOOD_OPTIONS.find(
                  (food) => Number(j.foodMenu) === food.id,
               ) || { icon: '🧑‍🍳' };
               return {
                  icon: userFood.icon,
                  userDisplayName: j.userDisplayName,
                  id: j.id,
                  keyArrivalTime: j.keyArrivalTime,
                  userEmail: j.userEmail,
               };
            }),
      };
   });

   return peoplePerDate;
};
