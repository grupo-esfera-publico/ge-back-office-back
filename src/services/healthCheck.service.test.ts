import { HealthCheckService } from './healthCheck.service';

describe('Health Check Service', () => {
   describe('Check Health', () => {
      it('Should return a status', async () => {
         const healthCheckService = new HealthCheckService();
         const healthCheckSpy = jest.spyOn(healthCheckService, 'checkHealth');
         const expectedResult = { status: "I'm alive" };
         expect(healthCheckService.checkHealth()).toEqual(expectedResult);
         expect(healthCheckSpy).toBeCalled();
         expect(healthCheckSpy).toHaveBeenCalledTimes(1);
      });
   });
});
