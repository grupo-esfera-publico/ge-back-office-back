import sgMail from '@sendgrid/mail';

export class EMailService {
   private sendGridApiKey: string;

   constructor() {
      this.sendGridApiKey = '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';
      sgMail.setApiKey(this.sendGridApiKey);
   }

   async sendEmail(message: string, emailTo: string): Promise<boolean> {
      const msg = {
         to: emailTo,
         from: 'notificaciones-covid@grupoesfera.com.ar',
         subject: 'Alerta posible contagio COVID',
         text: message,
      };

      try {
         await sgMail.send(msg);
         console.log('Enviado a ', emailTo);
         return true;
      } catch (error) {
         console.log('error envio ', error);
         return false;
      }
   }
}
