import { BookingsService } from './bookings.service';
import { ErrorMessages } from '../config/constants';

describe('Bookings Service Tests', () => {
   describe('createBooking', () => {
      const bookingsService = new BookingsService();
      it('Should create a new booking', () => {
         givenADateNumberOfBookingsBelowLimit();
         thenABookingIsCreatedSuccessfully(
            'Usuario',
            'email@mail.com',
            '23/2/22',
            '1',
            false,
            '',
         );
      });

      it('Should return a booking cap error', () => {
         givenADateWithBookingCapReached();
         thenABookingIsCreatedReturnsBookingCapError(
            'Usuario',
            'email@mail.com',
            '2022-01-25T14:55:50.000Z',
            '1',
            false,
            '',
         );
      });

      it('Should return a duplicate error', () => {
         givenADateWithASingleBooking();
         thenABookingIsCreatedReturnsDuplicateError(
            'Eric Aniduzzi',
            'eric.aniduzzi@grupoesfera.com.ar',
            '2022-01-25T14:55:50.000Z',
            '1',
            false,
            '',
         );
      });

      const givenADateWithBookingCapReached = () => {
         const response: any[] | PromiseLike<any[]> = [
            {
               weekDay: 'Martes',
               date: '25/1/2022',
               dateObject: { day: 25, month: 0, year: 2022 },
               assistants: [
                  {
                     icon: '🍖',
                     userDisplayName: 'Ignacio Raguet',
                     id: 'AadbEdukvegmzlZiX618',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Martin Mata',
                     id: 'AadbEdukvegmzlZiX619',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Guillermo Sfoggia',
                     id: 'AadbEdukvegmzlZiX610',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'María Laclau',
                     id: 'AadbEdukvegmzlZiX611',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Raysa Alanes',
                     id: 'AadbEdukvegmzlZiX612',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Mariano García',
                     id: 'AadbEdukvegmzlZiX613',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Mariano Tugnarelli',
                     id: 'AadbEdukvegmzlZiX613',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Magui García Cano',
                     id: 'AadbEdukvegmzlZiX614',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Leandro Nolasco',
                     id: 'AadbEdukvegmzlZiX615',
                  },
                  {
                     icon: '🍖',
                     userDisplayName: 'Eric Aniduzzi',
                     id: 'AadbEdukvegmzlZiX616',
                  },
               ],
            },
         ];
         bookingsService.getBookings = jest.fn(() => Promise.resolve(response));
      };

      const givenADateWithASingleBooking = () => {
         const response: any[] | PromiseLike<any[]> = [
            {
               weekDay: 'Martes',
               date: '25/1/2022',
               dateObject: { day: 25, month: 0, year: 2022 },
               assistants: [
                  {
                     icon: '🍖',
                     userDisplayName: 'Eric Aniduzzi',
                     id: 'AadbEdukvegmzlZiX616',
                  },
               ],
            },
         ];
         bookingsService.getBookings = jest.fn(() => Promise.resolve(response));
      };

      const givenADateNumberOfBookingsBelowLimit = () => {
         bookingsService.getBookings = jest.fn(() => Promise.resolve([]));
      };

      const thenABookingIsCreatedSuccessfully = (
         displayName: string,
         email: string,
         bookingDate: string,
         foodMenu: string,
         userHasKeys: boolean,
         keyArrivalTime: string,
      ) => {
         expect(() => {
            bookingsService.createBooking(
               displayName,
               email,
               bookingDate,
               foodMenu,
               userHasKeys,
               keyArrivalTime,
            );
         }).not.toThrow();
      };

      const thenABookingIsCreatedReturnsBookingCapError = async (
         displayName: string,
         email: string,
         bookingDate: string,
         foodMenu: string,
         userHasKeys: boolean,
         keyArrivalTime: string,
      ) => {
         try {
            await bookingsService.createBooking(
               displayName,
               email,
               bookingDate,
               foodMenu,
               userHasKeys,
               keyArrivalTime,
            );
         } catch (error: any) {
            expect(error.message).toEqual(
               ErrorMessages.BOOKING_CAP_REACHED_ERROR,
            );
         }
      };

      const thenABookingIsCreatedReturnsDuplicateError = async (
         displayName: string,
         email: string,
         bookingDate: string,
         foodMenu: string,
         userHasKeys: boolean,
         keyArrivalTime: string,
      ) => {
         try {
            await bookingsService.createBooking(
               displayName,
               email,
               bookingDate,
               foodMenu,
               userHasKeys,
               keyArrivalTime,
            );
         } catch (error: any) {
            expect(error.message).toEqual(
               ErrorMessages.DUPLICATE_BOOKING_ERROR,
            );
         }
      };
   });
});
