import {
   collection,
   addDoc,
   CollectionReference,
   DocumentData,
} from 'firebase/firestore';
import { db } from '../config/firebase-config';
import { BookingsService } from './bookings.service';
import { EMailService } from './mail.service';
import { MailStatus } from '../config/types';
import { convertDates } from '../utils/dates';

export class NotificationsService {
   private notificationsCollectionRef: CollectionReference<DocumentData>;
   private bookingsService: BookingsService;
   private emailService: EMailService;

   constructor() {
      this.notificationsCollectionRef = collection(db, 'notificaciones');
      this.bookingsService = new BookingsService();
      this.emailService = new EMailService();
   }

   public async sendNotification(
      bookingDate: Date,
      userDisplayName: string,
      notificationMessage: string,
   ): Promise<MailStatus[]> {
      const initialDate = new Date(bookingDate.getTime());
      const finalDate = new Date(bookingDate.getTime());
      initialDate.setHours(0, 0, 0);
      convertDates(initialDate); //Convert en vez de setearle un valor que suene raro como las 3
      finalDate.setHours(23, 59, 59);
      convertDates(finalDate);

      const pastBookings = await this.bookingsService.getUserBookings(
         initialDate,
         finalDate,
         userDisplayName,
      );

      const assistants = pastBookings[0].assistants.filter(
         (assistant) => assistant.userDisplayName !== userDisplayName,
      );

      const assistantsMails = assistants.map((assist) => assist.userEmail);

      const statusSendMail = await this.sendMail(
         assistantsMails,
         notificationMessage,
      );

      await addDoc(this.notificationsCollectionRef, {
         notificationMessage,
         assistantsMails,
         userDisplayName,
         bookingDate,
      });

      return statusSendMail;
   }

   private async sendMail(
      assistantsMails: string[],
      notificationMessage: string,
   ): Promise<MailStatus[]> {
      const statusSendMail: MailStatus[] = [];
      for (const assistantMail of assistantsMails) {
         const isSendOk = await this.emailService.sendEmail(
            notificationMessage,
            assistantMail,
         );

         statusSendMail.push({ mail: 'assistantMail', status: isSendOk });
      }
      return statusSendMail;
   }
}
