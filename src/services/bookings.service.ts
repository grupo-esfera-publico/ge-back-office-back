import {
   collection,
   doc,
   getDocs,
   addDoc,
   deleteDoc,
   CollectionReference,
   DocumentData,
} from 'firebase/firestore';
import { db } from '../config/firebase-config';
import {
   BookingsPerDate,
   FirebaseBooking,
   FirebaseDate,
} from '../config/types';
import { WEEK_DAYS, ErrorMessages } from '../config/constants';
import { formatterBookings } from '../formatters/formatter';
import { convertDates } from '../utils/dates';

export class BookingsService {
   private bookingsCollectionRef: CollectionReference<DocumentData>;

   constructor() {
      this.bookingsCollectionRef = collection(db, 'reservas');
   }

   private getYesterday() {
      const date = new Date();
      date.setDate(date.getDate() - 1);
      date.setHours(23, 59, 59);
      convertDates(date); //Convert en vez de setearle un valor que suene raro como las 2:59:59
      return date;
   }

   public async getUserBookings(since: Date, until: Date, userName: string) {
      const bookings = await this.getBookings(since, until);

      const userBookings = bookings.filter((booking) => {
         return booking.assistants.find(
            (userWithMenu) => userWithMenu.userDisplayName === userName,
         );
      });

      return userBookings;
   }

   public async getBookings(
      since?: Date,
      until?: Date,
   ): Promise<BookingsPerDate[]> {
      const firebaseData = await getDocs(this.bookingsCollectionRef);
      const bookingsData = firebaseData.docs.map(
         (doc) =>
            new FirebaseBooking(
               doc.id,
               doc.get('date'),
               doc.get('userDisplayName'),
               doc.get('foodMenu'),
               doc.get('keyArrivalTime'),
               doc.get('userEmail'),
            ),
      );

      const sortedBookingsData = bookingsData.sort(
         (a, b) => a.date.seconds - b.date.seconds,
      );

      const asOfTodayBookings = sortedBookingsData.filter((sbd) =>
         since && until
            ? this.fnGetDatesBetween(sbd.date, since, until)
            : this.fnGetCurrentDates(sbd.date),
      );

      const onlyDates = asOfTodayBookings.map((d) => {
         const tempDate = new Date(d.date.seconds * 1000);
         const weekDay = WEEK_DAYS.find(
            (wd) => wd.id === tempDate.getDay(),
         )?.weekDay;

         return {
            weekDay: weekDay,
            day: tempDate.getDate(),
            month: tempDate.getMonth(),
            year: tempDate.getFullYear(),
         };
      });

      const filteredDates = Array.from(
         new Set(onlyDates.map((od) => JSON.stringify(od))),
      ).map((set) => JSON.parse(set));

      const peoplePerDate: BookingsPerDate[] = formatterBookings(
         filteredDates,
         asOfTodayBookings,
      );

      return peoplePerDate;
   }

   fnGetCurrentDates = (date: FirebaseDate) => {
      return date.seconds * 1000 > Number(this.getYesterday());
   };

   fnGetDatesBetween = (date: FirebaseDate, since: Date, until: Date) => {
      return (
         date.seconds * 1000 >= Number(since) &&
         date.seconds * 1000 < Number(until)
      );
   };

   public async createBooking(
      displayName: string,
      email: string,
      bookingDate: string,
      selectedFoodMenu: string,
      userHasKeys: boolean,
      keyArrivalTime: string,
   ) {
      const bookings = await this.getBookings();
      const dateWithoutHour = bookingDate.split('T');
      const parsedDate = new Date(dateWithoutHour[0]);
      parsedDate.setHours(0, 0, 0);
      convertDates(parsedDate); //Convert en vez de setearle un valor que suene raro como las 3
      this.validateBooking(bookings, displayName, parsedDate);
      const newDoc = {
         userDisplayName: displayName,
         userEmail: email,
         date: parsedDate,
         foodMenu: selectedFoodMenu,
      };

      const newDocWithKeysValidation = userHasKeys
         ? { ...newDoc, keyArrivalTime: keyArrivalTime }
         : newDoc;

      await addDoc(this.bookingsCollectionRef, newDocWithKeysValidation);
   }

   public async deleteBooking(id: string) {
      try {
         const bookingDoc = doc(db, 'reservas', id);
         await deleteDoc(bookingDoc);
      } catch (error) {
         console.log('DELETE BOOKING SERVICE ERROR', error);
      }
   }

   private validateBooking(
      bookings: BookingsPerDate[],
      displayName: string,
      bookingDate: Date,
   ) {
      const selectedDateBookings = bookings.find((booking) => {
         return (
            booking.dateObject.day === bookingDate.getDate() &&
            booking.dateObject.month === bookingDate.getMonth() &&
            booking.dateObject.year === bookingDate.getFullYear()
         );
      });

      if (!selectedDateBookings) return;

      const searchAssistant = selectedDateBookings.assistants.find(
         (assistant) => assistant.userDisplayName === displayName,
      );

      if (selectedDateBookings.assistants.length === 10) {
         throw new Error(ErrorMessages.BOOKING_CAP_REACHED_ERROR);
      }

      if (searchAssistant)
         throw new Error(ErrorMessages.DUPLICATE_BOOKING_ERROR);
   }
}
