import express from 'express';
import { HealthCheckRoute } from './routes/healthCheck.route';
import { BookingsRoute } from './routes/bookings.route';
import cors from 'cors';

class App {
   public app: express.Application;
   public healthCheckRoute: HealthCheckRoute;
   public bookingsRoute: BookingsRoute;
   constructor() {
      this.app = express();
      this.app.use(express.json());
      this.app.use(express.urlencoded({ extended: true }));
      this.app.use(cors());
      this.healthCheckRoute = new HealthCheckRoute();
      this.healthCheckRoute.routes(this.app);
      this.bookingsRoute = new BookingsRoute();
      this.bookingsRoute.routes(this.app);
   }
}

export default new App().app;
