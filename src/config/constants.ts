export const WEEK_DAYS = [
   {
      id: 0,
      weekDay: 'Domingo',
   },
   {
      id: 1,
      weekDay: 'Lunes',
   },
   {
      id: 2,
      weekDay: 'Martes',
   },
   {
      id: 3,
      weekDay: 'Miércoles',
   },
   {
      id: 4,
      weekDay: 'Jueves',
   },
   {
      id: 5,
      weekDay: 'Viernes',
   },
   {
      id: 6,
      weekDay: 'Sábado',
   },
];

export const FOOD_OPTIONS = [
   {
      id: 1,
      description: 'morfo todo',
      icon: '🍖',
   },
   {
      id: 2,
      description: 'vegeta',
      icon: '🥕',
   },
   {
      id: 3,
      description: 'vegan',
      icon: '🥬',
   },
   {
      id: 4,
      description: 'sin TACC',
      icon: '🌮',
   },
   {
      id: 5,
      description: 'llevo mi propio almuerzo',
      icon: '🧑‍🍳',
   },
   {
      id: 6,
      description: 'vegeta + sin TACC',
      icon: '🥕🌮',
   },
];

export const ErrorMessages = {
   DUPLICATE_BOOKING_ERROR: 'Ya tenías reservado ese día',
   BOOKING_CAP_REACHED_ERROR: 'No hay cupos para este día',
   BOOKING_ERROR: 'Error en la reserva',
};
