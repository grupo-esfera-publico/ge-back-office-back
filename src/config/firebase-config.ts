import { initializeApp } from 'firebase/app';
import { getFirestore } from '@firebase/firestore';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
   apiKey: 'AIzaSyBa4nPrqLWwP4arj11Tg3nFFxUgBz40Tuw',
   authDomain: 'backoffice-ge-dev.firebaseapp.com',
   projectId: 'backoffice-ge-dev',
   storageBucket: 'backoffice-ge-dev.appspot.com',
   messagingSenderId: '548125743856',
   appId: '1:548125743856:web:a68dbd739b88992ea5c035',
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
export const authFirebase = getAuth(app);
