export type FirebaseDate = {
   seconds: number;
   nanoseconds: number;
};

export class FirebaseBooking {
   id: string;
   date: FirebaseDate;
   userDisplayName: string;
   foodMenu: number;
   keyArrivalTime: string;
   userEmail: string;

   constructor(
      id: string,
      date: FirebaseDate,
      userDisplayName: string,
      foodMenu: number,
      keyArrivalTime: string,
      userEmail: string,
   ) {
      this.id = id;
      this.date = date;
      this.userDisplayName = userDisplayName;
      this.foodMenu = foodMenu;
      this.keyArrivalTime = keyArrivalTime;
      this.userEmail = userEmail;
   }
}

type UserWithMenu = {
   icon: string;
   userDisplayName: string;
   id: string;
   keyArrivalTime?: string;
   userEmail: string;
};

type DateObject = {
   day: number;
   month: number;
   year: number;
};

export type BookingsPerDate = {
   weekDay: string;
   date: string;
   dateObject: DateObject;
   assistants: UserWithMenu[];
};

export type MailStatus = {
   mail: string;
   status: boolean;
};
