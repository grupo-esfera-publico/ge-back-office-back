import { BookingsService } from '../services/bookings.service';
import { Request, Response } from 'express';

export class BookingsController {
   private bookingsService: BookingsService;

   constructor() {
      this.bookingsService = new BookingsService();
   }

   public getBookingsService = (): BookingsService => {
      return this.bookingsService;
   };

   public getBookings = async (req: Request, res: Response) => {
      const bookings = await this.bookingsService.getBookings();
      return res.status(200).send(bookings);
   };

   public getPastBookings = async (req: Request, res: Response) => {
      const today = new Date();
      const todayMinus10Days = new Date();
      todayMinus10Days.setDate(todayMinus10Days.getDate() - 10);
      const userName: any = req.query.userName;

      const bookings = await this.bookingsService.getUserBookings(
         todayMinus10Days,
         today,
         userName,
      );
      return res.status(200).send(bookings);
   };

   public createBooking = async (req: Request, res: Response) => {
      const displayName = req.body.displayName;
      const email = req.body.email;
      const bookingDate = req.body.bookingDate;
      const foodMenu = req.body.selectedFoodMenu;
      const userHasKeys = req.body.userHasKeys;
      const keyArrivalTime = req.body.keyArrivalTime;

      if (!displayName || !bookingDate || !foodMenu) {
         return res.status(400).send('Missing required params');
      }

      try {
         await this.bookingsService.createBooking(
            displayName,
            email,
            bookingDate,
            foodMenu,
            userHasKeys,
            keyArrivalTime,
         );
         res.status(201).send('CREATED');
      } catch (err: any) {
         return res.status(500).send({ errorMessage: err.message });
      }
   };

   public deleteBookings = async (req: Request, res: Response) => {
      const id = req.params.id;
      await this.bookingsService.deleteBooking(id);
      return res.status(200).send('OK');
   };
}
