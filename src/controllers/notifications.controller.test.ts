import { NotificationsController } from './notifications.controller';
import { mockRequest } from 'mock-req-res';
jest.mock('../services/notifications.service');

const notificationsController = new NotificationsController();
const notificationsService = notificationsController.getNotificationsService();
const req = mockRequest({
   body: {
      userDisplayName: 'Nombre',
      bookingDate: '25/01/2022',
      notificationMessage: 'Mensaje',
   },
});

const mockResponse = () => {
   const res: any = {};
   res.status = jest.fn().mockReturnValue({
      send: jest.fn(),
   });
   return res;
};
const res = mockResponse();

describe('Notifications Controller Tests', () => {
   describe('Send Notifications Tests', () => {
      it('If all messages are sent should return a 201 message response', async () => {
         notificationsService.sendNotification = jest
            .fn()
            .mockResolvedValue([{ mail: 'mail@email.com', status: true }]);

         await notificationsController.sendNotification(req, res);

         expect(res.status).toHaveBeenCalledWith(201);
         expect(res.status().send).toHaveBeenCalledWith('CREATED');
      });

      it('If all messages are not should return a 500 message response', async () => {
         notificationsService.sendNotification = jest
            .fn()
            .mockResolvedValue([{ mail: 'mail@email.com', status: false }]);

         await notificationsController.sendNotification(req, res);

         expect(res.status).toHaveBeenCalledWith(500);
         expect(res.status().send).toHaveBeenCalledWith('FAILED');
      });

      it('If one message is not sent should return a 201 message response but with email failed', async () => {
         notificationsService.sendNotification = jest.fn().mockResolvedValue([
            { mail: 'mail@email.com', status: true },
            { mail: 'mail2@email.com', status: false },
         ]);

         await notificationsController.sendNotification(req, res);

         expect(res.status).toHaveBeenCalledWith(201);
         expect(res.status().send).toHaveBeenCalledWith(
            'FAILED:mail2@email.com',
         );
      });

      it('If two messages are not sent but one is sent should return a 201 message response but with two emails failed', async () => {
         notificationsService.sendNotification = jest.fn().mockResolvedValue([
            { mail: 'mail@email.com', status: false },
            { mail: 'mail2@email.com', status: true },
            { mail: 'mail3@email.com', status: false },
         ]);

         await notificationsController.sendNotification(req, res);

         expect(res.status).toHaveBeenCalledWith(201);
         expect(res.status().send).toHaveBeenCalledWith(
            'FAILED:mail@email.com,mail3@email.com',
         );
      });
   });
});
