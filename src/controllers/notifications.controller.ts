import { Request, Response } from 'express';
import { NotificationsService } from '../services/notifications.service';
import { MailStatus } from '../config/types';

export class NotificationsController {
   private notificationsService: NotificationsService;

   constructor() {
      this.notificationsService = new NotificationsService();
   }

   public getNotificationsService = (): NotificationsService => {
      return this.notificationsService;
   };

   public sendNotification = async (req: Request, res: Response) => {
      const userDisplayName = req.body.userDisplayName;
      const bookingDate = new Date(req.body.bookingDate);
      const notificationMessage = req.body.notificationMessage;

      const response: MailStatus[] =
         await this.notificationsService.sendNotification(
            bookingDate,
            userDisplayName,
            notificationMessage,
         );

      const allOk = response.every((element) => element.status === true);

      if (allOk) {
         return res.status(201).send('CREATED');
      } else {
         const allNotOk = response.every((element) => element.status === false);
         if (allNotOk) {
            return res.status(500).send('FAILED');
         }
         const notOk = response
            .filter((element) => element.status === false)
            .map((element) => element.mail);
         return res.status(201).send('FAILED:' + notOk.join(','));
      }
   };
}
