import { BookingsController } from './bookings.controller';
import { mockRequest } from 'mock-req-res';

describe('Bookings Controller Tests', () => {
   describe('Create Booking Tests', () => {
      it('With full booking data should return a 201 status', async () => {
         const bookingsController = new BookingsController();
         const bookingsService = bookingsController.getBookingsService();
         bookingsService.createBooking = jest.fn().mockResolvedValue({});

         const req = mockRequest({
            body: {
               displayName: 'Nombre',
               email: 'email@mail.com',
               bookingDate: '25/01/2022',
               selectedFoodMenu: '1',
               userHasKeys: true,
               keyArrivalTime: '09:00',
            },
         });

         const mockResponse = () => {
            const res: any = {};
            res.status = jest.fn().mockReturnValue({
               send: jest.fn(),
            });
            return res;
         };
         const res = mockResponse();

         await bookingsController.createBooking(req, res);

         expect(res.status).toHaveBeenCalledWith(201);
         expect(res.status().send).toHaveBeenCalledWith('CREATED');
      });
   });
});
