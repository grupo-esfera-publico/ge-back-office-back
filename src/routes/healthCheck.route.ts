import express from 'express';
import { HealthCheckController } from '../controllers/healthCheck.controller';

export class HealthCheckRoute {
   private healthCheckController: HealthCheckController;

   constructor() {
      this.healthCheckController = new HealthCheckController();
   }

   public routes(app: express.Application): void {
      app.route('/healthCheck').get(this.healthCheckController.checkHealth);
   }
}
