import { BookingsController } from '../controllers/bookings.controller';
import express from 'express';
import { NotificationsController } from '../controllers/notifications.controller';

export class BookingsRoute {
   private bookingsController: BookingsController;
   private notificationsController: NotificationsController;

   constructor() {
      this.bookingsController = new BookingsController();
      this.notificationsController = new NotificationsController();
   }

   public routes(app: express.Application): void {
      app.route('/bookings').get(this.bookingsController.getBookings);
      app.route('/bookings/past').get(this.bookingsController.getPastBookings);
      app.route('/bookings').post(this.bookingsController.createBooking);
      app.route('/bookings/:id').delete(this.bookingsController.deleteBookings);
      app.route('/notifications').post(
         this.notificationsController.sendNotification,
      );
   }
}
